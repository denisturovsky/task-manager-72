package ru.tsc.denisturovsky.tm.integration.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.dto.model.UserDTO;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.marker.WebIntegrationCategory;
import ru.tsc.denisturovsky.tm.model.Result;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;

@Category(WebIntegrationCategory.class)
public final class ProjectRestEndpointTest {

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private static final HttpHeaders HEADER = new HttpHeaders();

    @Nullable
    private static String SESSION_ID;

    @Nullable
    private static String USER_ID;

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO(USER_PROJECT2_NAME, USER_PROJECT2_DESCRIPTION);

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO(USER_PROJECT3_NAME, USER_PROJECT3_DESCRIPTION);

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        HEADER.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<Result> response = restTemplate.postForEntity(
                url, new HttpEntity<>(""), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = HttpCookie.parse(headersResponse.getFirst(HttpHeaders.SET_COOKIE));
        SESSION_ID = cookies.stream()
                            .filter(item -> "JSESSIONID".equals(item.getName())
                            ).findFirst().get().getValue();
        Assert.assertNotNull(SESSION_ID);
        HEADER.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + SESSION_ID));
        @NotNull final String urlProfile = "http://localhost:8080/api/auth/profile";
        @NotNull final ResponseEntity<UserDTO> responseProfile = restTemplate.exchange(
                urlProfile, HttpMethod.GET, new HttpEntity<>(HEADER), UserDTO.class);
        USER_ID = responseProfile.getBody().getId();
    }

    private static ResponseEntity<List> sendRequestList(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity<List> httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<ProjectDTO> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity<ProjectDTO> httpEntity
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, ProjectDTO.class);
    }

    @AfterClass
    public static void afterClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(HEADER));
    }

    @Test
    public void addTest() throws Exception {
        @NotNull final String url = BASE_URL + "add/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project3, HEADER));
        @NotNull final String findUrl = BASE_URL + "findById/" + project3.getId();
        ;
        Assert.assertNotNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody());
    }

    @After
    public void after() {
        @NotNull final String url = BASE_URL + "deleteAll/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(HEADER));
    }

    @Before
    public void before() {
        @NotNull final String url = BASE_URL + "add/";

        project1.setUserId(USER_ID);
        project1.setCreated(new Date());
        project1.setCreatedBy("test");
        project1.setUpdated(new Date());
        project1.setUpdatedBy("test");

        project2.setUserId(USER_ID);
        project2.setCreated(new Date());
        project2.setCreatedBy("test");
        project2.setUpdated(new Date());
        project2.setUpdatedBy("test");

        project3.setUserId(USER_ID);
        project3.setCreated(new Date());
        project3.setCreatedBy("test");
        project3.setUpdated(new Date());
        project3.setUpdatedBy("test");

        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project1, HEADER));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project2, HEADER));
    }

    @Test
    public void deleteAllTest() throws Exception {
        @NotNull final String url = BASE_URL + "deleteAll/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(HEADER));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void deleteTest() throws Exception {
        @NotNull final String url = BASE_URL + "delete/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project1, HEADER));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(1, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void findAllTest() throws Exception {
        @NotNull final String url = BASE_URL + "findAll/";
        Assert.assertEquals(2, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody().size());
    }

    @Test
    public void findByIdTest() throws Exception {
        @NotNull final String url = BASE_URL + "findById/" + project1.getId();
        Assert.assertNotNull(sendRequest(url, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody());
    }

    @Test
    public void saveTest() throws Exception {
        @NotNull final String url = BASE_URL + "save/";
        project1.setStatus(Status.IN_PROGRESS);
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(project1, HEADER));
        @NotNull final String findUrl = BASE_URL + "findById/" + project1.getId();
        Assert.assertEquals(Status.IN_PROGRESS, sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(HEADER)).getBody()
                                                                                                              .getStatus());
    }

}