package ru.tsc.denisturovsky.tm.integration.soap;

import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.tsc.denisturovsky.tm.api.endpoint.AuthEndpoint;
import ru.tsc.denisturovsky.tm.api.endpoint.TaskEndpoint;
import ru.tsc.denisturovsky.tm.client.soap.AuthSoapEndpointClient;
import ru.tsc.denisturovsky.tm.client.soap.TaskSoapEndpointClient;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.marker.WebIntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.tsc.denisturovsky.tm.constant.TaskTestData.*;

@Category(WebIntegrationCategory.class)
public class TasksSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private static AuthEndpoint authEndpoint;

    @NotNull
    private static TaskEndpoint taskEndpoint;

    @Nullable
    private static String USER_ID;

    @NotNull
    private final TaskDTO task1 = new TaskDTO(USER_TASK1_NAME, USER_TASK1_DESCRIPTION);

    @NotNull
    private final TaskDTO task2 = new TaskDTO(USER_TASK2_NAME, USER_TASK2_DESCRIPTION);

    @NotNull
    private final TaskDTO task3 = new TaskDTO(USER_TASK3_NAME, USER_TASK3_DESCRIPTION);

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("test", "test").getSuccess());
        taskEndpoint = TaskSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) taskEndpoint;
        @NotNull Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(
                MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        USER_ID = authEndpoint.profile().getId();
    }

    @Test
    public void addTest() throws Exception {
        taskEndpoint.add(task3);
        Assert.assertNotNull(taskEndpoint.findById(task3.getId()));
    }

    @After
    public void after() throws Exception {
        taskEndpoint.clear();
    }

    @Before
    public void before() throws Exception {
        task1.setUserId(USER_ID);
        task2.setUserId(USER_ID);
        task3.setUserId(USER_ID);
        taskEndpoint.add(task1);
        taskEndpoint.add(task2);
    }

    @Test
    public void deleteAllTest() throws Exception {
        taskEndpoint.clear();
        Assert.assertNull(taskEndpoint.findAll());
    }

    @Test
    public void deleteTest() throws Exception {
        taskEndpoint.delete(task1);
        Assert.assertNull(taskEndpoint.findById(task1.getId()));
    }

    @Test
    public void findAllTest() throws Exception {
        @Nullable final List<TaskDTO> tasks = taskEndpoint.findAll();
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    public void findByIdTest() throws Exception {
        Assert.assertNotNull(taskEndpoint.findById(task1.getId()));
    }

    @Test
    public void saveTest() throws Exception {
        task1.setStatus(Status.IN_PROGRESS);
        taskEndpoint.save(task1);
        @Nullable final TaskDTO task = taskEndpoint.findById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

}