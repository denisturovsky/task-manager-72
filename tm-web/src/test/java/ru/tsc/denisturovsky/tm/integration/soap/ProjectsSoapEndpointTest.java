package ru.tsc.denisturovsky.tm.integration.soap;

import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.tsc.denisturovsky.tm.api.endpoint.AuthEndpoint;
import ru.tsc.denisturovsky.tm.api.endpoint.ProjectEndpoint;
import ru.tsc.denisturovsky.tm.client.soap.AuthSoapEndpointClient;
import ru.tsc.denisturovsky.tm.client.soap.ProjectSoapEndpointClient;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.marker.WebIntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;

@Category(WebIntegrationCategory.class)
public class ProjectsSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private static AuthEndpoint authEndpoint;

    @NotNull
    private static ProjectEndpoint projectEndpoint;

    @Nullable
    private static String USER_ID;

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO(USER_PROJECT2_NAME, USER_PROJECT2_DESCRIPTION);

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO(USER_PROJECT3_NAME, USER_PROJECT3_DESCRIPTION);

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("test", "test").getSuccess());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        @NotNull Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(
                MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        USER_ID = authEndpoint.profile().getId();
    }

    @Test
    public void addTest() throws Exception {
        projectEndpoint.add(project3);
        Assert.assertNotNull(projectEndpoint.findById(project3.getId()));
    }

    @After
    public void after() throws Exception {
        projectEndpoint.clear();
    }

    @Before
    public void before() throws Exception {
        project1.setUserId(USER_ID);
        project2.setUserId(USER_ID);
        project3.setUserId(USER_ID);
        projectEndpoint.add(project1);
        projectEndpoint.add(project2);
    }

    @Test
    public void deleteAllTest() throws Exception {
        projectEndpoint.clear();
        Assert.assertNull(projectEndpoint.findAll());
    }

    @Test
    public void deleteTest() throws Exception {
        projectEndpoint.delete(project1);
        Assert.assertNull(projectEndpoint.findById(project1.getId()));
    }

    @Test
    public void findAllTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = projectEndpoint.findAll();
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void findByIdTest() throws Exception {
        Assert.assertNotNull(projectEndpoint.findById(project1.getId()));
    }

    @Test
    public void saveTest() throws Exception {
        project1.setStatus(Status.IN_PROGRESS);
        projectEndpoint.save(project1);
        @Nullable final ProjectDTO project = projectEndpoint.findById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

}