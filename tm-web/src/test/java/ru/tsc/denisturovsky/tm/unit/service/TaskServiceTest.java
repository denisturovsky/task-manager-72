package ru.tsc.denisturovsky.tm.unit.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.api.service.dto.ITaskDTOService;
import ru.tsc.denisturovsky.tm.configuration.DataBaseConfiguration;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.dto.model.TaskDTO;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.marker.WebUnitCategory;
import ru.tsc.denisturovsky.tm.util.UserUtil;

import java.util.List;

import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;
import static ru.tsc.denisturovsky.tm.constant.TaskTestData.*;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
@Category(WebUnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO(USER_PROJECT2_NAME, USER_PROJECT2_DESCRIPTION);

    @NotNull
    private final TaskDTO task1 = new TaskDTO(USER_TASK1_NAME, USER_TASK1_DESCRIPTION);

    @NotNull
    private final TaskDTO task2 = new TaskDTO(USER_TASK2_NAME, USER_TASK2_DESCRIPTION);

    @NotNull
    private final TaskDTO task3 = new TaskDTO(USER_TASK3_NAME, USER_TASK3_DESCRIPTION);

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private String userId;

    @Test
    @SneakyThrows
    public void addByUserIdTest() {
        taskService.addByUserId(userId, task3);
        Assert.assertNotNull(taskService.findOneByUserIdAndId(userId, task3.getId()));
    }

    @Test
    @SneakyThrows
    public void addTest() {
        taskService.add(task3);
        Assert.assertNotNull(taskService.findOneById(task3.getId()));
    }

    @After
    @SneakyThrows
    public void after() {
        taskService.clear();
        projectService.clear();
    }

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken
                token = new UsernamePasswordAuthenticationToken(USER_TEST_NAME, USER_TEST_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        task1.setUserId(userId);
        task2.setUserId(userId);
        task1.setProjectId(project1.getId());
        task2.setProjectId(project1.getId());
        projectService.add(project1);
        projectService.add(project2);
        taskService.add(task1);
        taskService.add(task2);
    }

    @Test
    @SneakyThrows
    public void changeTaskStatusByIdTest() {
        taskService.changeTaskStatusById(task1.getId(), Status.IN_PROGRESS);
        @Nullable final TaskDTO task = taskService.findOneById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    public void changeTaskStatusByUserIdAndIdTest() {
        taskService.changeTaskStatusByUserIdAndId(userId, task1.getId(), Status.IN_PROGRESS);
        @Nullable final TaskDTO task = taskService.findOneByUserIdAndId(userId, task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    public void clearByUserIdTest() {
        taskService.clearByUserId(userId);
        Assert.assertEquals(0, taskService.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void clearTest() {
        taskService.clear();
        Assert.assertEquals(0, taskService.count());
    }

    @Test
    @SneakyThrows
    public void countByUserIdTest() {
        Assert.assertEquals(2, taskService.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void countTest() {
        Assert.assertEquals(2, taskService.count());
    }

    @Test
    @SneakyThrows
    public void createTest() {
        @Nullable final TaskDTO task = taskService.create(userId, USER_TASK3_NAME);
        Assert.assertNotNull(taskService.findOneByUserIdAndId(userId, task.getId()));
    }

    @Test
    @SneakyThrows
    public void createWithDescriptionTest() {
        @Nullable final TaskDTO task = taskService.create(userId, USER_TASK3_NAME, USER_TASK3_DESCRIPTION);
        Assert.assertNotNull(taskService.findOneByUserIdAndId(userId, task.getId()));
    }

    @Test
    @SneakyThrows
    public void existsByIdTest() {
        Assert.assertFalse(taskService.existsById(""));
        Assert.assertTrue(taskService.existsById(task1.getId()));
    }

    @Test
    @SneakyThrows
    public void existsByUserIdAndIdTest() {
        Assert.assertFalse(taskService.existsByUserIdAndId(userId, ""));
        Assert.assertTrue(taskService.existsByUserIdAndId(userId, task1.getId()));
    }

    @Test
    @SneakyThrows
    public void findAllByProjectIdTest() {
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(project1.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findAllByUserIdAndProjectIdTest() {
        @Nullable final List<TaskDTO> tasks = taskService.findAllByUserIdAndProjectId(userId, project1.getId());
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findAllByUserIdTest() {
        @Nullable final List<TaskDTO> tasks = taskService.findAllByUserId(userId);
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @Nullable final List<TaskDTO> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findOneByIdTest() {
        @Nullable final TaskDTO task = taskService.findOneById(task1.getId());
        Assert.assertNotNull(task);
    }

    @Test
    @SneakyThrows
    public void findOneByUserIdAndIdTest() {
        @Nullable final TaskDTO task = taskService.findOneByUserIdAndId(userId, task1.getId());
        Assert.assertNotNull(task);
    }

    @Test
    @SneakyThrows
    public void removeByIdTest() {
        taskService.removeById(task1.getId());
        Assert.assertNull(taskService.findOneById(task1.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByUserIdAndIdTest() {
        taskService.removeByUserIdAndId(userId, task1.getId());
        Assert.assertNull(taskService.findOneByUserIdAndId(userId, task1.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByUserIdTest() {
        taskService.removeByUserId(userId, task1);
        Assert.assertNull(taskService.findOneByUserIdAndId(userId, task1.getId()));
    }

    @Test
    @SneakyThrows
    public void removeTest() {
        taskService.remove(task1);
        Assert.assertNull(taskService.findOneById(task1.getId()));
    }

    @Test
    @SneakyThrows
    public void updateByIdTest() {
        @Nullable final TaskDTO task = taskService.updateById(task1.getId(), USER_TASK3_NAME, USER_TASK3_DESCRIPTION);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), USER_TASK3_NAME);
        Assert.assertEquals(task.getDescription(), USER_TASK3_DESCRIPTION);
    }

    @Test
    @SneakyThrows
    public void updateByUserIdAndIdTest() {
        @Nullable final TaskDTO task = taskService.updateByUserIdAndId(
                userId, task1.getId(), USER_TASK3_NAME, USER_TASK3_DESCRIPTION);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getName(), USER_TASK3_NAME);
        Assert.assertEquals(task.getDescription(), USER_TASK3_DESCRIPTION);
    }

    @Test
    @SneakyThrows
    public void updateByUserIdTest() {
        task1.setStatus(Status.IN_PROGRESS);
        @Nullable final TaskDTO task = taskService.updateByUserId(userId, task1);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    public void updateProjectIdByIdTest() {
        taskService.updateProjectIdById(task1.getId(), project1.getId());
        Assert.assertNotNull(taskService.findOneById(task1.getId()));
        @Nullable final List<TaskDTO> tasks = taskService.findAllByProjectId(project1.getId());
        Assert.assertNotNull(tasks);
    }

    @Test
    @SneakyThrows
    public void updateProjectIdByUserIdAndIdTest() {
        taskService.updateProjectIdByUserIdAndId(userId, task1.getId(), project1.getId());
        Assert.assertNotNull(taskService.findOneByUserIdAndId(userId, task1.getId()));
        @Nullable final List<TaskDTO> tasks = taskService.findAllByUserIdAndProjectId(userId, project1.getId());
        Assert.assertNotNull(tasks);
    }

    @Test
    @SneakyThrows
    public void updateTest() {
        task1.setStatus(Status.IN_PROGRESS);
        @Nullable final TaskDTO task = taskService.update(task1);
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getStatus(), Status.IN_PROGRESS);
    }

}