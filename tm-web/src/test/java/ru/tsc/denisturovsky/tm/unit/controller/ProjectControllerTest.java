package ru.tsc.denisturovsky.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.configuration.*;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.marker.WebUnitCategory;
import ru.tsc.denisturovsky.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {
                ApplicationConfiguration.class,
                DataBaseConfiguration.class,
                SecurityWebApplicationInitializer.class,
                ServiceAuthenticationEntryPoint.class,
                WebApplicationConfiguration.class
        }
)
@Category(WebUnitCategory.class)
public class ProjectControllerTest {

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO(USER_PROJECT2_NAME, USER_PROJECT2_DESCRIPTION);

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO(USER_PROJECT3_NAME, USER_PROJECT3_DESCRIPTION);

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    @NotNull
    private WebApplicationContext wac;

    @Nullable
    private String userId;

    @After
    @SneakyThrows
    public void after() throws Exception {
        projectService.clear();
    }

    @Before
    public void before() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken
                token = new UsernamePasswordAuthenticationToken(USER_TEST_NAME, USER_TEST_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        projectService.add(project1);
        projectService.add(project2);
    }

    @Test
    @SneakyThrows
    public void createTest() {
        @NotNull final String url = "/project/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
               .andDo(print())
               .andExpect(status().is3xxRedirection());
        @Nullable final List<ProjectDTO> projects = projectService.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(3, projects.size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final String url = "/project/delete/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
               .andDo(print())
               .andExpect(status().is3xxRedirection());
        @Nullable final ProjectDTO project = projectService.findOneById(project1.getId());
        Assert.assertNotNull(project);
    }

    @Test
    @SneakyThrows
    public void editTest() {
        @NotNull final String url = "/project/edit/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
               .andDo(print())
               .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String url = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
               .andDo(print())
               .andExpect(status().isOk());
    }

}