package ru.tsc.denisturovsky.tm.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.denisturovsky.tm.configuration.DataBaseConfiguration;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.marker.WebUnitCategory;
import ru.tsc.denisturovsky.tm.repository.ProjectDTORepository;
import ru.tsc.denisturovsky.tm.util.UserUtil;

import java.util.List;
import java.util.Optional;

import static ru.tsc.denisturovsky.tm.constant.ProjectTestData.*;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DataBaseConfiguration.class})
@Category(WebUnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO(USER_PROJECT1_NAME, USER_PROJECT1_DESCRIPTION);

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO(USER_PROJECT2_NAME, USER_PROJECT2_DESCRIPTION);

    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private String userId;

    @After
    @SneakyThrows
    public void after() {
        projectRepository.delete(project1);
        projectRepository.delete(project2);
    }

    @Before
    @SneakyThrows
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken
                token = new UsernamePasswordAuthenticationToken(USER_TEST_NAME, USER_TEST_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        userId = UserUtil.getUserId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        projectRepository.save(project1);
        projectRepository.save(project2);
    }

    @Test
    @SneakyThrows
    public void countByUserIdTest() {
        Assert.assertEquals(2, projectRepository.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void deleteByUserIdAndIdTest() {
        projectRepository.deleteByUserIdAndId(userId, project1.getId());
        Assert.assertEquals(1, projectRepository.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void deleteByUserIdTest() {
        projectRepository.deleteByUserId(userId);
        Assert.assertEquals(0, projectRepository.countByUserId(userId));
    }

    @Test
    @SneakyThrows
    public void existByUserIdAndIdTest() {
        Assert.assertFalse(projectRepository.existByUserIdAndId("", project1.getId()));
        Assert.assertFalse(projectRepository.existByUserIdAndId(userId, ""));
        Assert.assertTrue(projectRepository.existByUserIdAndId(userId, project1.getId()));
    }

    @Test
    @SneakyThrows
    public void findByUserIdAndIdTest() {
        @NotNull final Optional<ProjectDTO> project = projectRepository.findByUserIdAndId(userId, project1.getId());
        Assert.assertEquals(project1.getId(), project.orElse(null).getId());
    }

    @Test
    @SneakyThrows
    public void findByUserIdTest() {
        @NotNull final List<ProjectDTO> projects = projectRepository.findByUserId(userId);
        Assert.assertEquals(2, projects.size());
    }

}