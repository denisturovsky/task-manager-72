package ru.tsc.denisturovsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.denisturovsky.tm.api.endpoint.ProjectEndpoint;
import ru.tsc.denisturovsky.tm.api.service.dto.IProjectDTOService;
import ru.tsc.denisturovsky.tm.dto.model.ProjectDTO;
import ru.tsc.denisturovsky.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.tsc.denisturovsky.tm.api.endpoint.ProjectEndpoint")
public class ProjectRestEndpointImpl implements ProjectEndpoint {

    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/add")
    public ProjectDTO add(
            @WebParam(name = "project", partName = "project")
            @RequestBody @NotNull final ProjectDTO project
    ) throws Exception {
        return projectService.addByUserId(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void clear() throws Exception {
        projectService.clearByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() throws Exception {
        return projectService.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull final ProjectDTO project) throws Exception {
        projectService.removeByUserId(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        projectService.removeByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        return projectService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectDTO> findAll() throws Exception {
        return projectService.findAllByUserId(UserUtil.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDTO findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") @NotNull final String id
    ) throws Exception {
        return projectService.findOneByUserIdAndId(UserUtil.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public ProjectDTO save(
            @WebParam(name = "project", partName = "project")
            @RequestBody @NotNull final ProjectDTO project
    ) throws Exception {
        return projectService.updateByUserId(UserUtil.getUserId(), project);
    }

}