package ru.tsc.denisturovsky.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.denisturovsky.tm.api.service.model.IProjectService;
import ru.tsc.denisturovsky.tm.api.service.model.IUserService;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.denisturovsky.tm.exception.field.*;
import ru.tsc.denisturovsky.tm.model.Project;
import ru.tsc.denisturovsky.tm.repository.ProjectRepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectService implements IProjectService {

    @Nullable
    @Autowired
    private ProjectRepository repository;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Override
    @Transactional
    public Project add(@NotNull final Project model) throws Exception {
        return repository.save(model);
    }

    @NotNull
    @Override
    @Transactional
    public Project addByUserId(
            @Nullable final String userId,
            @NotNull final Project model
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        model.setUser(userService.findOneById(userId));
        return repository.save(model);
    }

    @Override
    @Transactional
    public void changeProjectStatusById(
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.save(project);
    }

    @Override
    @Transactional
    public void changeProjectStatusByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneByUserIdAndId(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.save(project);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void clearByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUser(userService.findOneById(userId));
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

    @Override
    public int countByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return (int) repository.countByUser(userService.findOneById(userId));
    }

    @NotNull
    @Override
    @Transactional
    public Project create(
            @Nullable String userId,
            @Nullable final String name
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setUser(userService.findOneById(userId));
        project.setName(name);
        return repository.save(project);
    }

    @NotNull
    @Override
    @Transactional
    public Project create(
            @Nullable String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setUser(userService.findOneById(userId));
        project.setName(name);
        project.setDescription(description);
        return repository.save(project);
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public boolean existsByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return repository.existByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public List<Project> findAll() throws Exception {
        return repository.findAll();
    }

    @Nullable
    @Override
    public List<Project> findAllByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findByUser(userService.findOneById(userId));
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final Optional<Project> result = repository.findById(id);
        return result.orElse(null);
    }

    @Nullable
    @Override
    public Project findOneByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return null;
        @NotNull final Optional<Project> result = repository.findByUserAndId(userService.findOneById(userId), id);
        return result.orElse(null);
    }

    @Override
    @Transactional
    public void remove(@Nullable final Project model) throws Exception {
        if (model == null) return;
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByUserId(
            @Nullable final String userId,
            @Nullable final Project model
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        if (findOneByUserIdAndId(model.getUser().getId(), model.getId()) == null) return;
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserAndId(userService.findOneById(userId), id);
    }

    @Nullable
    @Override
    @Transactional
    public Project update(@Nullable final Project model) throws Exception {
        if (model == null) return null;
        return repository.save(model);
    }

    @Nullable
    @Override
    @Transactional
    public Project updateById(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project updateByUserId(
            @Nullable final String userId,
            @Nullable final Project model
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        if (findOneByUserIdAndId(model.getUser().getId(), model.getId()) == null) return null;
        model.setUser(userService.findOneById(userId));
        repository.save(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public Project updateByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneByUserIdAndId(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

}