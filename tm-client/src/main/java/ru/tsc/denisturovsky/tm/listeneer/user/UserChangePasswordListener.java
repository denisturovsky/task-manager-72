package ru.tsc.denisturovsky.tm.listeneer.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.denisturovsky.tm.dto.request.UserChangePasswordRequest;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.event.ConsoleEvent;
import ru.tsc.denisturovsky.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "Change user password";

    @NotNull
    public static final String NAME = "user-change-password";

    @Override
    @EventListener(condition = "@userChangePasswordListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ENTER NEW PASSWORD:]");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setNewPassword(password);
        userEndpoint.changePasswordUser(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
